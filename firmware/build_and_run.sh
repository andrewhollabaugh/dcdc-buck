#!/bin/bash

cargo build --release

echo "Upload? [y/n]"
read DO_UPLOAD
echo $DO_UPLOAD

NEWLINE='
'

#if [[ "$DO_UPLOAD" == "\n"]] || [[ "$DO_UPLOAD" == "y"]] || [[ "$DO_UPLOAD" == "Y"]]; then
if [ "$DO_UPLOAD" = "y" ]; then
  openocd -f ./openocd_upload.cfg
fi
