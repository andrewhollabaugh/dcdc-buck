#![no_std]
#![no_main]

extern crate panic_halt;

use core::fmt::Write as _;

use cortex_m_rt::entry;
use cortex_m::peripheral::SYST;

use stm32g0::stm32g07x;

use embedded_hal::digital::v2::{ToggleableOutputPin};
use embedded_hal::serial::{Read, /*Write*/};

use stm32g0xx_hal::rcc::RccExt;
use stm32g0xx_hal::gpio::GpioExt;
use stm32g0xx_hal::serial::{Config, SerialExt};
use stm32g0xx_hal::time::U32Ext;
use stm32g0xx_hal::timer::pwm::PwmExt;
use stm32g0xx_hal::hal::PwmPin;
use stm32g0xx_hal::analog::adc::{Precision, SampleTime, AdcExt};
use stm32g0xx_hal::hal::adc::OneShot;
use stm32g0xx_hal::hal::timer::CountDown;
use stm32g0xx_hal::timer::TimerExt;

enum ControlMode {
    OpenLoop,
    ClosedLoop,
}

#[entry]
fn main() -> ! {
    let peripherals = stm32g07x::Peripherals::take().unwrap();
    let mut core_peripherals = stm32g07x::CorePeripherals::take().unwrap();

    peripherals.RCC.iopenr.write(|w| w.iopben().set_bit().iopaen().set_bit());

    core_peripherals.SYST.set_reload(0x00ffffff);
    core_peripherals.SYST.clear_current();
    core_peripherals.SYST.enable_counter();

    let mut rcc = peripherals.RCC.constrain();

    let gpioa = peripherals.GPIOA.split(&mut rcc);
    let gpiob = peripherals.GPIOB.split(&mut rcc);
    let mut blinky_led = gpiob.pb2.into_push_pull_output();

    let uart_config = Config::default().baudrate(115200.bps());
    let mut uart = peripherals.USART1.usart(gpioa.pa9, gpioa.pa10, uart_config, &mut rcc).unwrap();

    let pwm = peripherals.TIM1.pwm(10.khz(), &mut rcc);
    let mut pwm_ch1 = pwm.bind_pin(gpioa.pa8);
    let max_duty = pwm_ch1.get_max_duty();

    let mut adc = peripherals.ADC.constrain(&mut rcc);
    adc.set_sample_time(SampleTime::T_80);
    adc.set_precision(Precision::B_12);
    let mut vout_adc_pin = gpiob.pb1.into_analog();

    let mut closed_loop_timer = peripherals.TIM17.timer(&mut rcc);
    //let closed_loop_timer_period = 50_000.us();
    let closed_loop_timer_period = 20_000.us();
    closed_loop_timer.start(closed_loop_timer_period);

    //let mut next_byte = NextRxByte::Mode;
    let mut control_mode = ControlMode::OpenLoop;
    let mut vout_set_point: f64 = 0.0;

    let kp: f64 = 40.0;
    let ki: f64 = 100.0;
    let kd: f64 = 10.0;
    let mut i_term: f64 = 0.0;
    let mut last_error: f64 = 0.0;
    let vout_fudge_mul: f64 = 0.95;
    let vout_fudge_add: f64 = 0.0;
    let mut vout: f64 = 0.0;

    let mut buf = [0; 2];
    let mut len = 0;

    loop {
        if len < 2 {
            while let Ok(rx_byte)  = uart.read() {
                buf[len] = rx_byte;
                len += 1;
                //writeln!(uart, "1 {} {} {}", rx_byte, buf[0], buf[1]).ok();
            }
        }
        if len == 2 {
            if buf[0] == 0xaa {
                control_mode = ControlMode::OpenLoop;
                let new_duty = buf[1] as u32 * max_duty as u32 / 255;
                pwm_ch1.set_duty(new_duty as u16);
                pwm_ch1.enable();
            }
            else if buf[0] == 0xbb {
                control_mode = ControlMode::ClosedLoop;
                vout_set_point = buf[1] as f64 / 255.0 * 7.0;
                pwm_ch1.enable();
            }
            else {
                writeln!(uart, "malformed packet").ok();
            }
            buf = [0; 2];
            len = 0;
        }

        if SYST::get_reload() - SYST::get_current() > 1_000_000 {
            core_peripherals.SYST.clear_current();
            blinky_led.toggle().ok();

            let d_float = pwm_ch1.get_duty() as f64 / max_duty as f64;
            match control_mode {
                ControlMode::OpenLoop => writeln!(uart, "open-loop    D = {:.3}    Vo = {:.3}", d_float, vout).ok(),
                ControlMode::ClosedLoop => writeln!(uart, "closed-loop    Vo_set = {:.3}    D = {:.3},\tVo = {:.3}", vout_set_point, d_float, vout).ok(),
            };
        }

        if u16::MAX as u32 - closed_loop_timer.get_current() < 5_000 {
            closed_loop_timer.reset();

            let vout_raw: u32 = adc.read(&mut vout_adc_pin).expect("adc read fail");
            let vout_raw_scaled = 3.3 / 3.18182 * vout_raw as f64;
            vout = vout_raw_scaled / 4096.0 * 7.0 * vout_fudge_mul + vout_fudge_add;

            match control_mode {
                ControlMode::OpenLoop => {}
                ControlMode::ClosedLoop => {
                    let error = vout_set_point - vout;

                    let p_term = kp * error;
                    i_term += ki * error;
                    let d_term = kd * (error - last_error);

                    let mut new_d: u16 = p_term as u16 + i_term as u16 + d_term as u16;
                    if new_d > max_duty {
                        new_d = max_duty;
                    }

                    pwm_ch1.set_duty(new_d);
                    last_error = error;
                }
            }
        }
    }
}
