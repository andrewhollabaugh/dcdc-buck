EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R_US R1
U 1 1 606735F0
P 5400 3250
F 0 "R1" H 5468 3296 50  0000 L CNN
F 1 "10" H 5468 3205 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_Power_L48.0mm_W12.5mm_P55.88mm" V 5440 3240 50  0001 C CNN
F 3 "~" H 5400 3250 50  0001 C CNN
	1    5400 3250
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R2
U 1 1 60673896
P 6000 3250
F 0 "R2" H 6068 3296 50  0000 L CNN
F 1 "10" H 6068 3205 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_Power_L48.0mm_W12.5mm_P55.88mm" V 6040 3240 50  0001 C CNN
F 3 "~" H 6000 3250 50  0001 C CNN
	1    6000 3250
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_SPDT SW1
U 1 1 606740FE
P 5400 2800
F 0 "SW1" V 5350 3050 50  0000 R CNN
F 1 "SW_SPDT" H 5500 2600 50  0000 R CNN
F 2 "switch-spdt:switch-spdt" H 5400 2800 50  0001 C CNN
F 3 "~" H 5400 2800 50  0001 C CNN
	1    5400 2800
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_SPDT SW2
U 1 1 606745DC
P 6000 2800
F 0 "SW2" V 5950 3050 50  0000 R CNN
F 1 "SW_SPDT" H 6100 2600 50  0000 R CNN
F 2 "switch-spdt:switch-spdt" H 6000 2800 50  0001 C CNN
F 3 "~" H 6000 2800 50  0001 C CNN
	1    6000 2800
	0    -1   -1   0   
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J1
U 1 1 60674C59
P 4900 3050
F 0 "J1" H 4900 2850 50  0000 C CNN
F 1 "PWR_IN" H 4900 3150 50  0000 C CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 4900 3050 50  0001 C CNN
F 3 "~" H 4900 3050 50  0001 C CNN
	1    4900 3050
	-1   0    0    1   
$EndComp
Wire Wire Line
	6000 3100 6000 3000
Wire Wire Line
	5400 3100 5400 3000
Wire Wire Line
	5100 2950 5100 2500
Wire Wire Line
	5100 2500 5300 2500
Wire Wire Line
	5300 2500 5300 2600
Wire Wire Line
	5300 2500 5900 2500
Wire Wire Line
	5900 2500 5900 2600
Connection ~ 5300 2500
Wire Wire Line
	5100 3050 5100 3500
Wire Wire Line
	5100 3500 5400 3500
Wire Wire Line
	6000 3500 6000 3400
Wire Wire Line
	5400 3400 5400 3500
Connection ~ 5400 3500
Wire Wire Line
	5400 3500 6000 3500
NoConn ~ 5500 2600
NoConn ~ 6100 2600
Text Label 5500 3500 0    50   ~ 0
GND
Text Label 5550 2500 0    50   ~ 0
+V
$EndSCHEMATC
